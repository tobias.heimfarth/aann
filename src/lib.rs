// Straight forward Amorphous Artificial Neural Network (aann) ticker

use fast_math;

#[derive(Debug, Clone, Copy)]
pub enum NodeKind {
    In,
    Out,
    Hidden,
}

pub struct Network {
    pub nodes: Vec<Node>,
    pub in_nodes: Vec<usize>,
    pub out_nodes: Vec<usize>,
    pub hidden_nodes: Vec<usize>,
    pub links: Vec<Link>,
}

#[derive(Debug, Clone, Copy)]
pub struct Node {
    pub value: f32,
    pub bias: f32,
    pub kind: NodeKind,
    pub output: f32,
}

impl Node {
    pub fn new(kind_str: &str, bias: f32) -> Self {
        let kind = match kind_str {
            "i" => NodeKind::In,
            "o" => NodeKind::Out,
            "h" => NodeKind::Hidden,
            _ => panic!("Invalid node kind: {}.", kind_str),
        };
        Self {
            value: 0.,
            bias,
            kind,
            output: 0.,
        }
    }

    pub fn update_output(&mut self) {
        self.output = transfer_mod_sigmoidal(self.value + self.bias);
    }
}

fn transfer_logical_threshold(x: f32) -> f32 {
    if x < 0. {
        0.
    } else if x < 1. {
        x
    } else {
        1.
    }
}

fn transfer_mod_sigmoidal(x: f32) -> f32 {
    // Modified sigmoidal transfer function from
    // "Evolving Neural Networks through Augmenting Topologies"
    // Stanley and Miikkulainen
    1. / (1. + fast_math::exp(-4.9 * x))
}

#[derive(Debug, Clone, Copy)]
pub struct Link {
    pub in_node_index: usize,
    pub out_node_index: usize,
    pub weight: f32,
}

impl Link {
    pub fn new(in_node_index: usize, out_node_index: usize, weight: f32) -> Self {
        Self {
            in_node_index,
            out_node_index,
            weight,
        }
    }
}

impl Network {
    pub fn new(mut nodes: Vec<Node>, links: Vec<Link>) -> Self {
        let mut in_nodes = Vec::new();
        let mut out_nodes = Vec::new();
        let mut hidden_nodes = Vec::new();
        for (i, node) in nodes.iter_mut().enumerate() {
            match node.kind {
                NodeKind::In => in_nodes.push(i),
                NodeKind::Out => out_nodes.push(i),
                NodeKind::Hidden => hidden_nodes.push(i),
            };
        }
        Self {
            nodes,
            in_nodes,
            out_nodes,
            hidden_nodes,
            links,
        }
    }

    pub fn input(&mut self, input: Vec<f32>) {
        for (i, input_value) in input.iter().enumerate() {
            self.nodes[self.in_nodes[i]].value = *input_value;
        }
    }

    pub fn clear_nodes_values(&mut self) {
        for node in self.nodes.iter_mut() {
            node.value = 0.
        }
    }

    pub fn output(&self) -> Vec<f32> {
        let mut output = Vec::new();
        for i in self.out_nodes.iter() {
            output.push(self.nodes[*i].value)
        }
        output
    }

    pub fn tick(&mut self) {
        for link in self.links.iter() {
            self.nodes[link.out_node_index].value +=
                link.weight * self.nodes[link.in_node_index].output;
        }
        for node in self.nodes.iter_mut() {
            node.update_output();
        }
    }

    pub fn in_tick_out(&mut self, input: Vec<f32>) -> Vec<f32> {
        // All in one function: accepts input, ticks, output.
        self.input(input);
        self.tick();
        let out = self.output();
        self.clear_nodes_values();
        out
    }
}
